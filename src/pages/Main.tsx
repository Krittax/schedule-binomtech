import { Chart } from "features/chart";

import chartsData from "data/charts.json";

function Main() {
	return (
		<>
			{chartsData.map((data, index) => (
				<Chart key={index} dataset={data} />
			))}
		</>
	);
}

export { Main };
