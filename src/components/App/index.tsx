import { Outlet } from "react-router-dom";

import styles from "./App.module.scss";

function App() {
	return (
		<div className={styles.wrapper}>
			<main>
				<section className={styles.mainSection}>
					<div className={styles.container}>
						<Outlet />
					</div>
				</section>
			</main>
		</div>
	);
}

export { App };
