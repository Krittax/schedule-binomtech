import { createBrowserRouter, Navigate } from "react-router-dom";

import { App } from "components/App";

import { Main } from "pages/Main";

export const router = createBrowserRouter([
	{
		path: "/",
		element: <App />,
		errorElement: <Navigate to="/" replace />,
		children: [
			{
				index: true,
				element: <Main />
			}
		]
	}
]);