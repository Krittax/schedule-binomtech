export const getSplittedNumber = (num: number) => {
	const stringConvertedNumber = num.toString();
	const numberLength = stringConvertedNumber.length;
	let spacePosIndex = numberLength % 3;

	if (numberLength < 4) {
		return stringConvertedNumber;
	}

	return stringConvertedNumber
		.split("")
		.map((letter, index) => {
			let result = letter;

			if (index === spacePosIndex) {
				result = (spacePosIndex === 0 ? "" : " ") + letter;
				spacePosIndex += 3;
			}

			return result;
		})
		.join("");
};
