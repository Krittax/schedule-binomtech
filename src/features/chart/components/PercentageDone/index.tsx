import { PercentageDoneProps } from "features/chart/types";

import styles from "./styles.module.scss"

function PercentageDone({ done, plan }: PercentageDoneProps) {
	const percentDone = Math.round(100 / (plan / done));

	return (
		<span
			className={styles.percentage}
		>
			{percentDone}%
		</span>
	);
}

export { PercentageDone };
