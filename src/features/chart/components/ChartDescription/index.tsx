import { getSplittedNumber } from "features/chart/utils/formatters";

import { ChartDescriptionProps } from "features/chart/types";

import styles from "./styles.module.scss";

function ChartDescription({ dataset }: ChartDescriptionProps) {
	const {
		currentDone: { done, plan },
		futurePlan: { prediction, plan: overallPlan },
	} = dataset;

	return (
		<div className={styles.wrap}>
			<div className={styles.descriptionRow}>
				<span className={styles.title}>ФАКТ / ПЛАН (НА ТЕКУЩУЮ ДАТУ)</span>
				<div>
					<span className={styles.doneNumber}>{getSplittedNumber(done)} т</span>
					<span className={styles.planNumber}> / </span>
					<span className={styles.planNumber}>{getSplittedNumber(plan)} т</span>
				</div>
			</div>
			<div className={styles.descriptionRow}>
				<span className={styles.title}>ПРОГНОЗ / ПЛАН</span>
				<div>
					<span className={styles.doneNumber}>{getSplittedNumber(prediction)} т</span>
					<span className={styles.planNumber}> / </span>
					<span className={styles.planNumber}>{getSplittedNumber(overallPlan)} т</span>
				</div>
			</div>
		</div>
	);
}

export { ChartDescription };
