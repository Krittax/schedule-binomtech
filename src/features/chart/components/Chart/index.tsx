import { ChartDescription } from "../ChartDescription";
import { PercentageDone } from "../PercentageDone";

import { ChartProps } from "features/chart/types";

import styles from "./styles.module.scss";

function Chart({ dataset }: ChartProps) {
	const {
		currentDone: { done },
		futurePlan: { plan },
	} = dataset;

	return (
		<div className={styles.chart}>
			<div className={styles.chartStroke}>
				<div className={styles.blankPart}></div>
				<div className={styles.filledPart}></div>
				<div className={styles.chartCenter}></div>
			</div>
			<PercentageDone done={done} plan={plan} />
			<ChartDescription dataset={dataset} />
		</div>
	);
}

export { Chart };
