import chartsData from "data/charts.json";

export type ChartData = typeof chartsData[number];

export interface ChartProps {
	dataset: ChartData;
}

export interface PercentageDoneProps {
	done: number;
	plan: number;
}

export interface ChartDescriptionProps {
	dataset: ChartData;
}
